package com.example.tugasandroidui;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;

public class MainActivity4 extends AppCompatActivity{

    private CheckBox cbBrow, cbEye, cbNose, cbMouth;
    private ImageView dBrow, dEye, dNose, dMouth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main4);

        // untuk membuat face construction
        cbBrow = findViewById(R.id.checkBox1);
        cbEye = findViewById(R.id.checkBox2);
        cbNose = findViewById(R.id.checkBox3);
        cbMouth = findViewById(R.id.checkBox4);

        dBrow = findViewById(R.id.brow);
        dEye = findViewById(R.id.eye);
        dNose = findViewById(R.id.nose);
        dMouth = findViewById(R.id.mouth);

        boolean isBrowChecked = cbBrow.isChecked();
        boolean isEyeChecked = cbEye.isChecked();
        boolean isNoseChecked = cbNose.isChecked();
        boolean isMouthChecked = cbMouth.isChecked();

        updateBrow(isBrowChecked);
        updateEye(isEyeChecked);
        updateNose(isNoseChecked);
        updateMouth(isMouthChecked);

        cbBrow.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener(){
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isBrowChecked) {
                updateBrow(isBrowChecked);
            }
        });

        cbEye.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener(){
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isEyeChecked) {
                updateEye(isEyeChecked);
            }
        });

        cbNose.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener(){
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isNoseChecked) {
                updateNose(isNoseChecked);
            }
        });

        cbMouth.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener(){
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isMouthChecked) {
                updateMouth(isMouthChecked);
            }
        });
    }

    private void updateBrow(boolean isBrowChecked) {
        if (isBrowChecked) {
            dBrow.setVisibility(View.VISIBLE);
        } else {
            dBrow.setVisibility(View.INVISIBLE);
        }
    }

    private void updateEye(boolean isEyeChecked) {
        if (isEyeChecked) {
            dEye.setVisibility(View.VISIBLE);
        } else {
            dEye.setVisibility(View.INVISIBLE);
        }
    }

    private void updateNose(boolean isNoseChecked) {
        if (isNoseChecked) {
            dNose.setVisibility(View.VISIBLE);
        } else {
            dNose.setVisibility(View.INVISIBLE);
        }
    }

    private void updateMouth(boolean isMouthChecked) {
        if (isMouthChecked) {
            dMouth.setVisibility(View.VISIBLE);
        } else {
            dMouth.setVisibility(View.INVISIBLE);
        }
    }
}